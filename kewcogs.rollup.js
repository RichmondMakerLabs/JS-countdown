/**
 * Rollup - class
 *
 * Display sequence of elements whereby the first N elements
 *    (where N is configurable) display normally but should
 *    there be > N+1 elements then only one such element is
 *    displayed at a time and each subsequent item is 'rolled
 *    up' to replace it in sequence.
 *
 *    Requires jQuery
 */

(function($){

/**
 * Rollup - constructor
 *
 * create an instance of Rollup
 *
 * Note: This constructor uses the 'options hash' pattern to
 *    optionaly override default parameters. Examples:
 *
 *       // accept defaults
 *       new Rollup();
 *
 *       // override just 'fixed' parameter
 *       new Rollup({fixed: 2})  // override just 'fixed'
 *
 *       // override 'rollup_speed' and 'css_prefix'
 *       new Rollup({
 *             rollup_speed: "fast",
 *             css_prefix: "myprefix"});
 *
 * @param {dictionary} options a dictionary of options which,
 *    if present, override default parameters. These options
 *    are:-
 *       fixed: integer (default = 1) max number of
 *              non-rolling elements
 *       rollup_speed: "fast", "slow" or integer(millisecs)
 *              (default = "slow") speed of transition.
 *       rollup_interval: integer (default = 3000) interval
 *              in millisecs between rollup transitions
 *       css_prefix: string (default = "rollupdisp") prefix
 *              used to construct CSS class names for
 *              elements used to control display. This has
 *              been made an option so as to avoid conflict,
 *              particularly between different instances of
 *              Rollup used on same page,
 *
 */
Rollup = function (options){

   // use options hash pattern to set defaults
   $.extend(this, $.extend({
         fixed: 1,
         css_prefix: "rollupdisp",
         rollup_speed: "slow",
         rollup_interval: 3000,
      }, options||{}))

   // css selectors and classes specific to Rollup displays
   this.dynamic_class = this.css_prefix + "-dyn"
   this.current_class = this.css_prefix + "-cur"
   this.prev_class = this.css_prefix + "-prv"
   this.next_class = this.css_prefix + "-nxt"
}

/**
 * start
 *
 * must be called to activate rollup behaviour
 *
 */
Rollup.prototype.start = function (){
   var rlup = this;

   // make sure any previous timer is killed
   this.stop()

   // set up interval timer to repeatedly run the 'rollup'
   this.timer = setInterval(runAnim, this.rollup_interval)

   // Local functions called by timer.

   // re-designates which element to be displayed after
   // the next 'rollup' action
   function advanceCurrent(idx, container){
      container = $(container)
      var children = container.children();
      var prev = container.find("." + rlup.next_class)
      var prev_pos = children.index(prev)
      var nxt_pos = (prev_pos + 1) % children.length

      // unset any old previous elements
      container.find("." + rlup.prev_class)
         .removeClass(rlup.prev_class)

      // make what was next into previous
      prev.removeClass(rlup.next_class)
          .addClass(rlup.prev_class);

      // set the new next element
      children.eq(nxt_pos)
         .addClass(rlup.next_class)
         .css("bottom", "-100%")

   }

   // run the 'rollup' animation on designated elements
   function runAnim(){
      // setup each matching display section
      $("." + rlup.dynamic_class).each(advanceCurrent)

      $("." + rlup.prev_class)
         .animate({bottom: "100%"},rlup.rollup_speed)
      $("." + rlup.next_class)
         .animate({bottom: "0%"}, rlup.rollup_speed)
   }

}

/**
 * stop
 *
 * stops rollup behaviour
 *
 */
Rollup.prototype.stop = function(){
   if(this.timer)
      clearInterval(this.timer);
   this.timer = null;
}

/**
 * display
 *
 * creates a rollup-enabled display of elements
 * @param {jQuery container element} container element on
 *       page in which to display rollup
 * @param {list jQuery elements} elements an optional sequence
 *       of elements to be displayed. If undefined or null
 *       the existing contents of 'container' are used,
 *       otherwise these elements will replace the contents
 *       of 'container'.
 */
Rollup.prototype.display = function (container, elements){

   // if no 'elements' are provided the use existing
   // contents of 'container'
   if(! elements){
      elements = container.children()
   }

   container.empty(); // remove any existing content

   if(elements.length <= (this.fixed + 1))
      // not enough content to need rollup - just
      // add to 'container' normally
      container.append(elements)
   else{
      // add the static portion of the contents normally
      container.append(elements.slice(0,this.fixed));

      // create a <div> element to hold the rolled-up content
      var dynamics = $("<div>")
         .addClass(this.dynamic_class)
         .append(elements.slice(this.fixed))
         .css({
            "position": "relative",
            "overflow-x": "hidden",
            // "height": container.children().first().height()
         })
      dynamics.children().css({
            "position": "absolute",
            "bottom": "-100%"
         })
         .first()
            .addClass(this.next_class)
            .css("bottom", "0%")
      container.append(dynamics)

      // adjust display of dynamic elements
      this.adjustDisplay(container)
   }
};

/**
* adjustDisplay
*
* Resize the dynamic portion of the display based on
* the size of the static siblings. This is called when
* the contents of the display are changed, and may also
* be called to reformat e.g. after window size change.
*
* @param {jQuery container element} container element on
*       page in which to display rollup
*/
Rollup.prototype.adjustDisplay = function(container){
   var dynamics = $(container).find('.'+this.dynamic_class);
   var h = $(container).children().first().height()
   dynamics.css({"height" : h})
}

})(jQuery);
