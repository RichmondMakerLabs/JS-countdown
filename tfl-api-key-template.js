/**
 * Credentials for TFL unified api
 */

var/** @const */ TFL_ID = '<tfl api id here>'
var/** @const */ TFL_KEY = '<tfl api key here>'
