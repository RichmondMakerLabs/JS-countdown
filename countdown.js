

var CREDENTIALS

/**
 * monitorStops
 *
 * Schedule monitoring of bus arrival times for one or
 * more bus stops.
 *
 * @param {string or wrapped set} busstops elements in
 *    document within which bus arrivals are to be displayed.
 *    The elements' ids are used as stop id when querying
 *    the Transport For London API
 */
function monitorStops(busstops){

   busstops = $(busstops)

   // check that an api key is set
   if(!("TFL_ID" in window) || !("TFL_KEY" in window)){
      busstops.text("No API key set")
      return
   }

   CREDENTIALS = 'app_id=' + TFL_ID + '&app_key=' + TFL_KEY


   var stopIds = busstops.map(
      function(){return this.id}).get()

   busstops.each(function(){
      var txt = $(this).text()
      $(this).text("").addClass("stop_data")
      .append($('<div/>')  // add line to show stop details
               .addClass('stop_designator')
              .text(txt))
               // .text(this.text()))
      .append($('<div/>')
               .addClass('arrivals_rollup'))
   })

   queryStopDetails(stopIds)

   // setup Rollup display
   var rollup = new Rollup();
   rollup.start()

   // schedule initial query
   for(stop in stopIds){
      queryArrivals(stopIds[stop], rollup)
   }

   // adjust rollup display after window Resize
   $(window).resize(function(){
      $('.arrivals_rollup').each(function(idx, container){
         rollup.adjustDisplay(container)
      })
   })

}


/**
* queryArrivals
*
* make api call to retrieve bus arrival data
*
* @param {string} stop_id api bus stop designator
* @param {Rollup} rollup instance of Rollup to control
*      display of arrivals data
*
*/
queryArrivals = function(stop_id, rollup){
   var url = 'https://api.tfl.gov.uk/StopPoint/' + stop_id + '/Arrivals?' + CREDENTIALS

   $.ajax(url, {
      datatype: "json",
      success: function(data){
         var parent = $('#' + stop_id + " > .arrivals_rollup")
         parent.children().remove()  // remove stale data lines
         for(i in sortedArrivals(data)){
            // parent.append($('<p>' + (1 - (-i)) + " " + busDueString(data[i]) + '</p>'))
            parent.append(busDueLine(data[i], Number(i) + 1))
         }

         // rollup
         if(rollup)
            rollup.display(parent)
      },
      error: function(xhr){
         console.log("Ajax call in queryArrivals:  " + xhr.statusText)
      },
      complete: function(){
         // schedule refresh/retry in 30 seconds
         setTimeout(function(){ queryArrivals(stop_id, rollup)}, 30000)
      }
   })
}

/**
 * queryStopDetails
 *
 * make api call to get details of a bus stop
 * @param {[string]} stops list of stop ids
 */
queryStopDetails = function(stops){
   var url = 'https://api.tfl.gov.uk/StopPoint/' + stops.join() + '?' + CREDENTIALS
   $.ajax(url, {
      datatype: 'json',
      success: function(data){
         if (Array.isArray(data))
            updateStopDetails(data, stops)
         else
            updateStopDetails([data], stops)
      },
      error: function(xhr){
         console.log("Ajax call in queryStopDetails:  " + xhr.statusText)
         // retry in 30 seconds
         setTimeout(function()
            {queryStopDetails(stops)},
            30000)
      }
   })
}

updateStopDetails = function(details, stops){
   for (d in details){
      var ch = details[d]['children']
      for (c in ch){
         var id = ch[c]['id']
         if (stops.indexOf(id) >= 0){
            var name = ch[c]['commonName']
            var towards = ""
            var additional = ch[c]['additionalProperties']
            for (i in additional){
               if(additional[i]['key'] == "Towards"){
                  towards = " towards " + additional[i]['value']
               }
            }
            // update element
            $('#' + id + ">." + "stop_designator").text(name + towards)

         }
      }
   }
}

/**
 * busDueLine
 *
 * Generate <div> containing details of bus arrival prediction
 * @param {dictionary} data JSON object cotaining bus arrival details
 * @param {integer} seqNo sequence number
 */
busDueLine = function(data, seq){
   var route = data['lineName']
   var dest = " to " + data['destinationName'] + " "
   var eta = Math.trunc(data['timeToStation'] / 60)
   var when_due = eta <= 0 ? "due" : eta + " min"

   return $('<div/>')
      .addClass('arrival_line')
      .append($('<span/>')
         .addClass('arrival_seq')
         .text(seq.toString() + " "))
      .append($('<span/>')
         .addClass('arrival_route')
         .text(route))
      .append($('<span/>')
         .addClass('arrival_dest')
         .text(dest))
      .append($('<span/>')
         .addClass('arrival_due')
         .text(when_due))
}

/**
 * sortedArrivals
 *
 * arrivals data array sorted on ascending time-to-arrival
 * @param {array} arrivals details array
 * @return {array}
 */
sortedArrivals = function(arrivals){
   return arrivals.sort(function(a,b){return a['timeToStation'] - b['timeToStation']})
}
